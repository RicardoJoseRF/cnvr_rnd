package cnvr;

import java.io.DataInput;
import java.io.DataOutput;
import org.jgroups.Global;
import org.jgroups.Header;
import org.jgroups.util.Streamable;

/**
 * 
 * @author Doris Ana Sangeap
 * @author Ignacio Domínguez Martínez-Casanueva
 * @author Ricardo José Ruiz Fernández
 *
 */
public class MyHeader extends Header implements Streamable {
	
	public static final int HELLO_MSG = 1;
	public static final int BYE_MSG = 2;
	public static final int DISTANCE_MSG = 3;
	public static final int ADD_NODE_MSG = 4;
	public static final int NEIGHBOUR_MSG = 5;
	public static final int SEND_TABLE_MSG = 6;
	public static final int NEW_DATA_MSG = 7;
	public static final int SEND_DATA_MSG = 8;
	public static final int REP_DATA_MSG = 9;
	public static final int START_CHECK_DATA_MSG = 10;
	public static final int CHECK_DATA_MSG = 11;
	public static final int ADD_CHECK_DATA_MSG = 12;
	public static final int REMOVE_CHECK_DATA_MSG = 13;
	public static final int INTRODUCE_YOURSELF_MSG = 14;
	public static final int SEND_LEFT_MSG = 15;
	public static final int SEND_RIGHT_MSG = 16;
	public static final int REMOVE_DATA_MSG = 17;
	public static final int RM_ALL_MSG = 18;
	public static final int CHECK_OR_SEND_DELETE_MSG = 19;
	public static final int CHECK_OR_SEND_SHOW_MSG = 20;
	public static final int SHOW_VALUE_MSG = 21;

	
	
	public static final short magicID = (short)1900;
    private int type;

    public MyHeader() {
    }

    public MyHeader(int type) {
        this.type=type;
    }

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public void readFrom(DataInput arg0) throws Exception {
		type = arg0.readInt();
		
	}

	@Override
	public void writeTo(DataOutput arg0) throws Exception {
		arg0.writeInt(type);
		
	}

	@Override
	public int size() {
		return Global.INT_SIZE;
	}

}