package cnvr;

import java.util.List;
import java.util.Map;

import org.jgroups.Address;

/**
 * 
 * @author Doris Ana Sangeap
 * @author Ignacio Domínguez Martínez-Casanueva
 * @author Ricardo José Ruiz Fernández
 *
 */
public class Node {
	
	private Address address;
	private Integer key;
	private HashTable table;
	private DataDHT dataTable;
		
	/**
	 * Nodo del sistema distribuido
	 * 
	 * @param address Dirección Address que identifica al nodo
	 */
	public Node(Address address){
		this.table = new HashTable();
		this.dataTable = new DataDHT();
		this.table.addNode(address);
		this.address = address;
		this.key = table.getMain();
	}
	
	/**
	 * A�ade a la tabla de datos del nodo los datos propios de ese nodo
	 * El identificador del dato sera: numeroAleatorio+main o 
	 * numeroAleatorio+0+main si main<0 (ver fileIdGen() de DataDHT)
	 * @param data informacion del dato
	 * @return TRUE si el dato se ha a�adido y FALSE en caso contrario
	 */
	public Boolean addOwnData(int gen, String data) {
		return dataTable.addNewData(gen, key, data);
	}
	
	/**
	 * Metodo pensado para a�adir a la tabla de datos del nodo los datos 
	 * propios de los nodos de su tabla de nodos. 
	 * Por eso, el identificador esta previamente generado. El identificador 
	 * de los datos propios del nodo tendr�n como ultimos dos caracteres el 
	 * numero del nodo. Si no son propios, tendr�n como ultimos dos caracteres
	 * los del nodo al que pertenecen dentro de la tabla de nodos
	 * @param data informacion del dato
	 * @return TRUE si el dato se ha a�adido y FALSE en caso contrario
	 */
	public Boolean addNearData(String id, String data) {
		return dataTable.addExistingData(id, data);
	}
	
	public List<Address> addNode(Address ipAddress){
		return table.addNode(ipAddress);
	}
	
	public boolean removeNode(Address ipAddress){
		return table.removeNode(ipAddress);
	}
	
	public void receiveHT(HashTable ht){
		table.receiveHT(ht);
	}
		
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Integer getKey() {
		return key;
	}
	public void setKey(Integer key) {
		this.key = key;
	}

	public HashTable getTable() {
		return table;
	}
	
	public DataDHT getDataTable() {
		return dataTable;
	}

	public void setTable(HashTable table) {
		this.table = table;
	}

	public List<Integer> getArrayIds() {
		return table.getArrayIds();
	}

	public Object getMain() {
		return table.getMain();
	}

	public Map<Integer, Address> getMap() {
		return table.getMap();
	}

	public Boolean checkValue(Integer integer) {
		return dataTable.checkValue(integer);		
	}

	public Map<String, String> getMyDataMap(Object main) {
		return dataTable.getMyDataMap(main);
	}	
}
