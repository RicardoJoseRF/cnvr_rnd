package cnvr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.jgroups.Address;

import cnvr.Neighbours;

/**
 * 
 * @author Doris Ana Sangeap
 * @author Ignacio Domínguez Martínez-Casanueva
 * @author Ricardo José Ruiz Fernández
 *
 */
public class DataDHT {
	
	private Map<String, String> map;
	private List<String> ids;
	
	public DataDHT() {
		this.map = new HashMap<String, String>();
		this.ids = new ArrayList<String>();
	}
	
	
	
	/**
	 * Genera un numero aleatorio entre max y min como primera parte del 
	 * identificador del dato
	 * @return el numero aleatorio generado
	 */
	public int fileIdGen() {
		Random rn = new Random();
		int max = 99;
		int min = 0;
		return rn.nextInt(max - min + 1) + min;
	}
	
	/**
	 * A�ade un dato nuevo a la tabla y devuelve TRUE si el dato se ha a�adido y 
	 * FALSE en caso contrario. 
	 * Se usa para a�adir a la tabla de datos de un nodo los datos propios de ese 
	 * nodo.
	 * @param key es la segunda parte del identificador del dato y se corresponde
	 * con el main del nodo a cuya tabla pertenece. Siempre tiene dos d�gitos, por
	 * eso si es menor que 10 se le a�ade un 0 delante
	 * @param data informacion que contiene dicho dato
	 * @return TRUE si el dato se ha a�adido y FALSE en caso contrario
	 */
	public Boolean addNewData(int gen, Integer key, String data) {
		Boolean added = false;
		if (data != null) {
			String id = ""+gen;
			if (key < 10) id = id+"-"+0+key;
			else id = id+"-"+key;
			map.put(id, data);
			ids.add(id);
			added = true;
		}
		return added;
	}
	
	/**
	 * A�ade a una tabla un dato con identificador previamente generado.
	 * Se usar� para a�adir a la tabla de datos de un nodo, los datos propios de 
	 * los nodos de su tabla de nodos.
	 * @param id identificador previamente generado
	 * @param data informacion que contiene dicho dato
	 * @return TRUE si el dato se ha a�adido y FALSE en caso contrario
	 */
	public Boolean addExistingData(String id, String data) {
		Boolean added = false;
		if(id.split("-")[1].length()==1){
			id = id.split("-")[0] + "-0" + id.split("-")[1]; 
		}
		if (data != null) {
			map.put(id, data);
			ids.add(id);
			added = true;
		}
		return added;
	}
	
	public Boolean removeCopies(String nodeId) {
		Boolean b = false;
		System.out.println("Los dato a eliminar son los del nodo "+nodeId);
		for (String i: ids) {
			String[] id = i.split("-");
			String number = id[1];
			if (number.startsWith("0")) number = number.substring(1);
			System.out.println("El dato pertenece al nodo "+number+" como principal" );
			if (number==nodeId) {
				map.remove(i);
				ids.remove(i);
				b = true;
			} else {
				b = false;
			}
		}
		return b;
	}
	
	public String returnData(String dataId) {
		String s = null;
		Boolean b = false;
		for (String i: ids) {
			String[] id = i.split("-");
			String number = id[0];
			if (Integer.parseInt(number)==Integer.parseInt(dataId) && !b) {
				s = map.get(i);
				b = true;
			} 
		}
		return s;
	}
	
	public Map<String, String> returnMap(String nodeId) {
		Map<String, String> h = new HashMap<String, String>();
		for (String i: ids) {
			String[] id = i.split("-");
			String number = id[1];
			if (number.startsWith("0")) number = number.substring(1);
			if (Integer.parseInt(number)==Integer.parseInt(nodeId)) {
				h.put(i, map.get(i));
			} 
		}
		return h;
	}
	
	
	public void printDict(){		
		System.out.println("DATATABLE:   " + (map.toString()) + "  (Tamaño:"+String.valueOf(map.size())+")");		
	}

	
	
	public Boolean checkValue(int key){
				
		Set<Map.Entry<String, String>> set = map.entrySet();
		
		for (Map.Entry<String, String> me : set) {
			  
			int value = Integer.parseInt(me.getKey().substring(0, me.getKey().indexOf('-')));
			  
			if (value == key){ return true;	}
			  
		}
		// System.out.println("LA key es "+String.valueOf(key)+" y el map es " + map.toString());
		return false;
		
	}


	public Map<String, String> getMyDataMap(Object main){

		
		Map<String,String> valuesMap = new HashMap<String,String>();
		
		Set<Map.Entry<String, String>> set = map.entrySet();

		for (Map.Entry<String, String> me : set) {
		  String dataKey = me.getKey().substring(me.getKey().indexOf('-') + 1);
		  
		  if (dataKey.startsWith("0")){dataKey = dataKey.substring(dataKey.indexOf("0") + 1);}	
 
		  if (dataKey.equals( String.valueOf(main) ) ){
			  valuesMap.put(me.getKey(), me.getValue());
		  }
		}
		return valuesMap;
	}
	
	
	public List<Integer> getDataKeys(List<Integer> lint, Object main) {
		
		List<Integer> listToCheck = new ArrayList<Integer>();
		
		Set<Map.Entry<String, String>> set = map.entrySet();

		for (Map.Entry<String, String> me : set) {
		  String dataKey = me.getKey().substring(me.getKey().indexOf('-') + 1);
		  
		  if (dataKey.startsWith("0")){dataKey = dataKey.substring(dataKey.indexOf("0") + 1);}	
		  
		  for (int i= 0; i <lint.size(); i++){ if (lint.get(i) == null){ lint.set(i, -1);}}	  
 
		  if (dataKey.equals( String.valueOf(main) ) ){
			  listToCheck.add(Integer.parseInt(me.getKey().substring(0, me.getKey().indexOf('-'))));
		  
		  }

		  for (int i= 0; i <lint.size(); i++){ if (lint.get(i) == -1){ lint.set(i, null);}}	  


		}
		
		System.out.println("Soy el central de : " + listToCheck.toString());
		return listToCheck;
		
	}

	public void checkRemoveData(List<Integer> lint) {
		
		Set<Map.Entry<String, String>> set = map.entrySet();
		List<String> keysToDelete = new ArrayList<String>();
		
		// Cambiar null por -1
		for (int i= 0; i <lint.size(); i++){ if (lint.get(i) == null){ lint.set(i, -1);}}	  

		for (Map.Entry<String, String> me : set) {
		  
			
		  String dataKey = me.getKey().substring(me.getKey().indexOf('-') + 1);
		  
		  if (dataKey.startsWith("0")){dataKey = dataKey.substring(dataKey.indexOf("0") + 1);}
		  
		  Boolean deleteValue = true;
		  
		  for (int i= 0; i <lint.size(); i++){
			  if((lint.get(i).toString().equals(dataKey))||(lint.get(i).toString().equals("-1"))){
				  deleteValue = false;
			  }			  
		  }
		  
		  // System.out.println("Borramos a:	"+me.getKey()+" es igual a "+deleteValue.toString());
		  
		  if(deleteValue == true){
			  keysToDelete.add(me.getKey());
		  }
		  
		}
		
		// Cambiar -1 por null
		for (int i= 0; i <lint.size(); i++){ if (lint.get(i) == -1){ lint.set(i, null);}}	  

		for (int i= 0; i <keysToDelete.size(); i++){
			map.remove(keysToDelete.get(i));
			ids.remove(keysToDelete.get(i));
		}

		


		
	}
	
	public void removeDataFromNode(String id){
		for (int i= 0; i <ids.size(); i++){
				if(ids.get(i).startsWith(id+"-")){
					map.remove(ids.get(i));
					ids.remove(ids.get(i));
				}
		}
	}
	
	public Map<String,String> getDataMap() {
		return map;
	}
	
}