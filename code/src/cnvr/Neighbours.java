package cnvr;

import java.util.List;

/**
 * 
 * @author Doris Ana Sangeap
 * @author Ignacio Domínguez Martínez-Casanueva
 * @author Ricardo José Ruiz Fernández
 *
 */
public class Neighbours {

	public static int maxValue = 99;
		
	/**
	 * Calcula la distancia entre dos claves
	 * 
	 * @param val1 Primera clave
	 * @param val2 Segunda clave
	 * @return La distancia existente entre ambas claves
	 */
	public static Integer distance(int val1, int val2) {
		int falseDistance = Math.abs(val1 - val2);
		int limit = (int) maxValue / 2;

		if (falseDistance < limit) {
			return falseDistance;
		} else {
			return (maxValue - falseDistance);
		}
	}
	
	/**
	 * Comprueba si la clave dada se encuentra dentro del rango de valores de la tabla
	 * 
	 * @param val Clave a comprobar
	 * @param extremoIzda Extremo izquierdo de la tabla
	 * @param extremoDcha Extremo derecho de la tabla
	 * @return Booleano indicando si se encuentra dentro o no
	 */
	public static boolean containedIn(int val, int extremoIzda, int extremoDcha){
		
		if ( (extremoIzda > maxValue) || (extremoDcha > maxValue) || (extremoIzda < 0) || (extremoDcha < 0) ){
			return false;
		}
		
		else{
			// Ej: entre 3 (izda) y 9 (dcha)
			if (extremoDcha > extremoIzda){
				// Ej: 5 > 3 y 5 < 9
				if ( (val >= extremoIzda) && (val <= extremoDcha)){
					return true;
				}
				// Ej: 11 -> False
				else{
					return false;
				}
			
			}
			// Ej: entre 60 y 5 (pasamos el maxValue, que es 63 en los ejemplos)
			else{
				// Ej: 61 > 60 y 61 < 63  ..o... 3 < 5 y 3 < 63
				if ( (val >= extremoIzda) && (val <= maxValue) || (val <= extremoDcha) && (val <= maxValue) ){
					return true;
				}
				// Ej: 
				else{
					return false;
				}
			}
			
		} 
	}
	
	/**
	 * Dado un nodo y una lista devuelve el extremo de la tabla más próximo al nodo
	 * 
	 * @param newNode Clave del nuevo nodo
	 * @param nodes Lista de nodos
	 * @return Vecino de la tabla más proximo al nodo entrante
	 */
	public static int nearestNode(Integer newNode, List<Integer> nodes) {

		int distance = distance(newNode, nodes.get(0));
		int idx = 0;

		for (int c = 1; c < nodes.size(); c++) {
			int newDistance = distance(newNode, nodes.get(c));
			if (newDistance < distance) {
				idx = c;
				distance = newDistance;
			}
		}
		return nodes.get(idx);
	}
	

	public int getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}
	
	
	
	
}
