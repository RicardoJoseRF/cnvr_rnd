package cnvr;

import java.io.BufferedReader;
import cnvr.Neighbours;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgroups.*;
import org.jgroups.conf.ClassConfigurator;
import org.jgroups.util.Util;


/**
 *
 * @author Doris Ana Sangeap
 * @author Ignacio Domínguez Martínez-Casanueva
 * @author Ricardo José Ruiz Fernández
 *
 */
public class DHT_Logic extends ReceiverAdapter{


	/**
	 *
	 * VERY IMPORTANT INFORMATION:
	 *
	 * Note that anything that could block should not be done in a callback.
	 * This includes sending of messages; if we have FLUSH on the stack,
	 * and send a message in a viewAccepted() callback, then the following happens:
	 * the FLUSH protocol blocks all (multicast) messages before installing a view,
	 * then installs the view, then unblocks.
	 * However, because installation of the view triggers the viewAccepted() callback,
	 * sending of messages inside of viewAccepted() will block.
	 * This in turn blocks the viewAccepted() thread, so the flush will never return!
	 * If we need to send a message in a callback, the sending should be done on a separate thread,
	 * or a timer task should be submitted to the timer.
	 *
	 */

	private View oldView;
	private JChannel channel;
	private Node nodo;
	private int distance;
	private int winner;
	private boolean winDraw=true;
	private boolean quorum = false;
	private boolean addedNode = false;
	private boolean addedData = false;
	private boolean returnedData = false;
	private Address newMember;
	private Integer newData = null;
	private Integer newRequest = null;
	private Address client = null;
	private String data;
	private int counter = 0;

	private void start() throws Exception{

		ClassConfigurator.add(MyHeader.magicID, MyHeader.class);
		channel = new JChannel();
		//channel.setDiscardOwnMessages(true);

		// En caso de ejecución local le pasaremos la dirección a pelo para inventarnos la IP.
		//Node nodo = new Node(new IpAddress(InetAddress.getLocalHost().getHostAddress()));
		eventLoop();
		channel.close();
	}

	/**
	 * Bucle del proceso principal.
	 */
	private void eventLoop(){
	    BufferedReader in=new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.print("Introduzca su dirección: ");
            String line=in.readLine();
    		channel.setName(line);
    		channel.connect("MyCluster");
    		channel.setReceiver(this);
            nodo = new Node (channel.getAddress());
            oldView = channel.getView(); //Initialize oldView

            sendMessage(null, MyHeader.HELLO_MSG, nodo.getKey());
            System.out.println("Hello everyone!");
            System.out.println("Waiting for more nodes..");
            while(true){

            	if(in.ready()){ //Block mainLoop if user starts typing
            		line = in.readLine().toLowerCase();
            		if(line.startsWith("quit") || line.startsWith("exit")){
            			System.out.println("Leaving cluster...");
                        break;
            		} else if (line.startsWith("info")) {
						System.out.println("Showing info...");
						showNodeInfo();
					} else if (line.startsWith("data")) {
            			showMyData();
					} else if (line.startsWith("rm-one")) {
						String id = line.split(" ")[1];
						nodo.getDataTable().removeDataFromNode(id);
						showMyData();
            		} else if (line.startsWith("remove")) {
	            		String id = line.split(" ")[1];
	            		sendMessage(nodo.getAddress(), MyHeader.CHECK_OR_SEND_DELETE_MSG, id);		
					} else if (line.startsWith("show")) {
	            		
						String id = line.split(" ")[1];

	            		
	            		List<Object> id_addr = new ArrayList<Object>();
	            		id_addr.add(id);
	            		id_addr.add(nodo.getAddress());
	            		
	            		sendMessage(nodo.getAddress(), MyHeader.CHECK_OR_SEND_SHOW_MSG, id_addr);			
	        		} else if (line.startsWith("add")){
            			Integer dataId = nodo.getDataTable().fileIdGen();
            			sendMessage(null, MyHeader.NEW_DATA_MSG, dataId);
            			sendMessage(null, MyHeader.SEND_DATA_MSG, line.split(" ")[1]);
            		}
            	}

            	Thread.sleep(100);
            	if(quorum){
            		// System.out.println("Entering quorum state");
            		setTimeOut();
            		quorumProcess();
            	}
            }
            sendMessage(null, MyHeader.BYE_MSG, nodo.getKey());
            Thread.sleep(2000); //Long wait for polite goodbye
        }
        catch(Exception e) {}

	}

	
	/**
	 * Override del método viewAccepted.
	 * Este callback se encarga de notificar cambios en la lista de miembros del grupo.
	 */
	public void viewAccepted(View new_view) {
	    System.out.println("Los miembros de la view son:" + new_view.getMembers().toString());
	    checkClusterStatus(new_view);
	}

	/**
	 * Override del método receive.
	 * Este callback se encarga de escuchar cualquier mensaje recibido.
	 */
	public void receive(Message msg) {
		MyHeader hdr = (MyHeader)msg.getHeader(MyHeader.magicID);
		switch(hdr.getType()){

			case MyHeader.HELLO_MSG: 			helloHandler(msg);
									 			break;

			case MyHeader.DISTANCE_MSG:			distanceHandler(msg);
												break;

			case MyHeader.ADD_NODE_MSG:			addNodeHandler(msg);
												break;

			case MyHeader.NEIGHBOUR_MSG:		updateNeighbourHandler(msg);
												break;

			case MyHeader.SEND_TABLE_MSG:		sendTableHandler(msg);
												break;

			case MyHeader.NEW_DATA_MSG:			newDataHandler(msg);
												break;

			case MyHeader.SEND_DATA_MSG:		sendDataHandler(msg);
												break;

			case MyHeader.REP_DATA_MSG:			replicateDataHandler(msg);
												break;

			case MyHeader.START_CHECK_DATA_MSG:	startCheckDataHandler(msg);
												break;

			case MyHeader.CHECK_DATA_MSG:		checkDataHandler(msg);
												break;

			case MyHeader.ADD_CHECK_DATA_MSG: 	addCheckDataHandler(msg);
												break;

			case MyHeader.REMOVE_CHECK_DATA_MSG:removeCheckDataHandler(msg);
												break;

			case MyHeader.INTRODUCE_YOURSELF_MSG:	introduceYourselfHandler(msg);
													break;
													
			case MyHeader.SEND_LEFT_MSG:			sendLeftHandler(msg);
													break;

			case MyHeader.SEND_RIGHT_MSG:			sendRightHandler(msg);
													break;

			case MyHeader.REMOVE_DATA_MSG:			removeDataHandler(msg);
													break;
			
			case MyHeader.RM_ALL_MSG:				rmAllHandler(msg);
													break;		
			
			case MyHeader.BYE_MSG:					leaveClusterHandler(msg);
													break;	
													
			case MyHeader.CHECK_OR_SEND_DELETE_MSG:	checkOrSend_Delete(msg);
													break;	
																							
			case MyHeader.CHECK_OR_SEND_SHOW_MSG:	checkOrSend_Show(msg);
													break;	
								
			case MyHeader.SHOW_VALUE_MSG:			showValue(msg);
													break;	
													
		}
	}


	/**
	 * Método para borrar una ID y enviar mensajes a los de alrededor para que lo hagan también
	 * 
	 * @param id
	 */
	public void removeID(String id){
		
		Map<Integer, Address> actualMap = nodo.getTable().getMap();
		List<Integer> actualIds= nodo.getTable().getArrayIds();
		Map<String,String> datatable = nodo.getDataTable().getDataMap();
		
		int claveNodo = 0;
		for ( String key : datatable.keySet() ) {
			if(key.startsWith(id)){
				claveNodo = Integer.valueOf(key.split("-")[1]);
				break;
			}
		}
		if(claveNodo == 0){System.out.println("Valor a borrar no encontrado");}
		for ( Integer key : actualMap.keySet() ) {
			try{
				if(key == claveNodo){
					sendMessage(actualMap.get(key), MyHeader.RM_ALL_MSG, id);
				}
			}catch(Exception e){}
		}
	}
	
	
	/**
	 * Comprueba que la ID esté dentro de los límites de su HT y llama a que se borre, si no lo reenvía
	 * 
	 * @param msg
	 */
	public void checkOrSend_Delete(Message msg){
		
		String id = null;
		try {
			id = (String) Util.objectFromByteBuffer( msg.getBuffer() );
		} catch (Exception e) {}
		
		Map<Integer, Address> actualMap = nodo.getTable().getMap();
		List<Integer> actualIds= nodo.getTable().getArrayIds();
		
		boolean contained = false;
		try{
			if(Neighbours.containedIn(Integer.valueOf(id),actualIds.get(0),actualIds.get(2* (nodo.getTable().getEle() )))){
				contained = true;
			}
		}catch(Exception e){
			contained = true;
		}
		
			
		if(contained){
			System.out.println("Valor a borrar contenido");
			removeID(id);
		}
		else{
			System.out.println("Valor a borrar no contenido. Reenviando a  "+String.valueOf ( actualMap.get(actualIds.get(0) ) ));
			sendMessage(actualMap.get(actualIds.get(0)), MyHeader.CHECK_OR_SEND_DELETE_MSG, id);
		}
			
	}
	
	/**
	 * Recibe en un mensaje el valor del ID y lo muestra
	 * @param msg
	 */
	public void showValue(Message msg){
		String value = null;
		try {
			value = (String) Util.objectFromByteBuffer( msg.getBuffer() );
		} catch (Exception e) {}
		if(value == null){
			System.out.println("El valor no existe");
		}
		else{
			System.out.println("Valor del ID:	" + value );
		}
	}
	
	/**
	 * Comprueba que la ID esté dentro de los límites de su HT y llama a que se muestre, si no lo reenvía
	 * 
	 * @param msg
	 */
	public void checkOrSend_Show(Message msg){
		
		System.out.println("Check or send SHOW received");

		List<Object> id_addr = null;
		String id = null;

		try {
			id_addr = (List<Object>) Util.objectFromByteBuffer( msg.getBuffer() );
			id = (String) id_addr.get(0);
		} catch (Exception e1) {}
				
		Map<Integer, Address> actualMap = nodo.getTable().getMap();
		List<Integer> actualIds= nodo.getTable().getArrayIds();
		
		boolean contained = false;
		try{
			if(Neighbours.containedIn(Integer.valueOf(id),actualIds.get(0),actualIds.get(2* (nodo.getTable().getEle() )))){
				contained = true;
			}
		}catch(Exception e){
			contained = true;
		}
					
		if(contained){
			String value = nodo.getDataTable().returnData(id);
			Address dest = (Address) id_addr.get(1);
			sendMessage(dest, MyHeader.SHOW_VALUE_MSG, value);
		}
		else{
			System.out.println("Valor a mostrar no contenido. Reenviando a  "+String.valueOf(actualMap.get(actualIds.get(0)) ));
			
			
			sendMessage(actualMap.get(actualIds.get(0)), MyHeader.CHECK_OR_SEND_SHOW_MSG, id_addr);
		}
			
	}
	
	
	/**
	 * Nuevo Thread a través del cual enviamos mensaje NEIGHBOUR MSG
	 */
	private void quorumProcess(){
		Thread quorumThread = new Thread(){
		    public void run(){
		    	try {

		    		quorum = false;
            		counter = 0;
            		// System.out.println("Quorum process has ended");


            		// ACTUALIZACION DE TABLAS
            		if(distance!=0 && winner==distance && winDraw && (newMember != null)){
						if (nodo.getTable().tableIsComplete()) {
							Map<Integer, Address> originalMap= nodo.getTable().getMap();
							List<Integer> originalIds= nodo.getTable().getArrayIds();						
							List<Address> result = nodo.addNode(newMember);
							Thread.sleep(50);
							Address excludedNode = result.get(0);
							
							// Sacamos la posición
							Map<Integer, Address> actualMap = nodo.getTable().getMap();
							List<Integer> actualIds= nodo.getTable().getArrayIds();
							
							int key_ = 0;
							for ( Integer key : actualMap.keySet() ) {
								if(actualMap.get(key) == newMember){
									key_ = key;
								}
							}
							
							int pos_ = 0;
							boolean found = false;
							for (int i=0; i<actualIds.size(); i++){
								if (key_ == actualIds.get(i)){
									found = true;
								}
								if(found){break;}
								pos_ ++;
							}
							
							// Hay un excluido -> Mando mi tabla, actualizo la del otro vecino y le digo que la mande él también
							int ele = nodo.getTable().getEle();
							if (ele == 1){
								sendMessage(newMember, MyHeader.NEIGHBOUR_MSG,Util.objectToByteBuffer(nodo.getTable()));				
								sendMessage(excludedNode, MyHeader.ADD_NODE_MSG, newMember);
								Thread.sleep(200);
								sendMessage(excludedNode, MyHeader.SEND_TABLE_MSG, newMember);
								sendMessage(newMember, MyHeader.START_CHECK_DATA_MSG,"");
							}
							else{
								
								if (excludedNode == null){
									sendMessage(newMember, MyHeader.NEIGHBOUR_MSG,Util.objectToByteBuffer(nodo.getTable()));
								}
								
								else{
									if(pos_ <= ele){
										sendMessage(originalMap.get(originalIds.get(pos_)), MyHeader.SEND_TABLE_MSG, newMember);
										sendMessage(originalMap.get(originalIds.get(pos_+1)), MyHeader.SEND_TABLE_MSG, newMember);
									}
									else{
										sendMessage(originalMap.get(originalIds.get(pos_)), MyHeader.SEND_TABLE_MSG, newMember);
										sendMessage(originalMap.get(originalIds.get(pos_-1)), MyHeader.SEND_TABLE_MSG, newMember);
									}
									sendMessage(newMember, MyHeader.INTRODUCE_YOURSELF_MSG, null);	
									sendMessage(newMember, MyHeader.START_CHECK_DATA_MSG,"");
								}
							
							}
							
							
							
						} else {
							nodo.addNode(newMember);
							sendMessage(newMember, MyHeader.NEIGHBOUR_MSG, Util.objectToByteBuffer(nodo.getTable()));
							sendMessage(newMember, MyHeader.START_CHECK_DATA_MSG,"");
						}

                    }


            		// INTERCAMBIO DE DATOS
					if (newData==null && newRequest==null) {
						if (distance != 0 && winner == distance && winDraw) {
							System.out.println("I got the nearest distance...");
							addedNode = true;

							if (nodo.getTable().tableIsComplete()) {
								Map<String, String> m = nodo.getDataTable().returnMap(nodo.getKey().toString());
								for (String i: m.keySet()) {
									sendMessage(newMember, MyHeader.REP_DATA_MSG, i+"-"+m.get(i));
								}
							} else {
								Map<String, String> m = nodo.getDataTable().returnMap(nodo.getKey().toString());
								for (String i: m.keySet()) {
									sendMessage(newMember, MyHeader.REP_DATA_MSG, i+"-"+m.get(i));
								}
							}
						}

						// If table is incomplete, add new node anyway
						else if (!addedNode && !nodo.getTable().tableIsComplete() && nodo.getAddress() != newMember) {
							nodo.addNode(newMember);
							Map<String, String> m = nodo.getDataTable().returnMap(nodo.getKey().toString());
							for (String i: m.keySet()) {
								sendMessage(newMember, MyHeader.REP_DATA_MSG, i+"-"+m.get(i));
							}
							addedNode = true;
						}
						addedNode = false;

					} else if (client==null && newRequest==null) {
						if (distance != 0 && winner == distance && winDraw) {
							addedData = true;
							nodo.getDataTable().addNewData(newData, nodo.getKey(), data);
							Map<Integer, Address> m = nodo.getTable().getMap();
							for ( Integer i : m.keySet() ) {
								if (i!=nodo.getKey()) sendMessage(m.get(i), MyHeader.REP_DATA_MSG, newData+"-"+nodo.getKey());
							}
							newData=null;

						}

						addedData=false;
					} else if (newRequest!=null) {
						if (distance != 0 && winner == distance && winDraw && !returnedData) {
							String d = nodo.getDataTable().returnData(newRequest.toString());

							if (d!=null) {
								sendMessage(client, MyHeader.SEND_DATA_MSG, d);
								returnedData=true;
								newRequest=null;
							}
						}
					}
				} catch (Exception e) {}
				showNodeInfo();
		    }
		};
		quorumThread.start();
	}

	/**
	 * Método auxiliar que imprime por pantalla los valores de:
	 *  - IDs de los nodos de la HashTable
	 *  - Contenido de la Hashtable (nodos)
	 *  - Contenido de la Datatable (datos)
	 */
	private void showNodeInfo(){
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {}
		System.out.println("");
		System.out.println("  --- NODE INFO ---			");
		nodo.getTable().printArrayIds();
		nodo.getTable().printDict();
		nodo.getDataTable().printDict();
		System.out.println("");
	}
	
	/**
	 * 
	 * Método auxiliar para sacar por pantalla los datos del nodo
	 * 
	 */
	private void showMyData(){
		nodo.getDataTable().printDict();
	}

	/**
	 * Método que procesa cambios en la vista del cluster
	 * Por el momento sólo comprueba caídas en la red
	 *
	 * @param new_view Nueva vista generada
	 */
	private void checkClusterStatus(View new_view){
		// System.out.println("Checking cluster status");
		Thread checkClusterThread = new Thread(){
		    public void run(){
		    	try {
		    		int newViewSize = new_view.size();
		    		if(newViewSize == oldView.size() - 1){
		    			for(Address member: oldView.getMembers()){
		    				if(!new_view.containsMember(member)){
		    					System.out.println("Se ha caído el nodo "+member);
		    					try{
		    						int ele = nodo.getTable().getEle();
		    						
		    						if(ele == 1){
			    						nodo.removeNode(member);
		    							if (! nodo.getTable().tableIsComplete()) {
			    							sendMessage(null, MyHeader.ADD_NODE_MSG, nodo.getAddress());
			    						}
		    						}
		    						

		    						else{

		    							if (nodo.getTable().tableIsComplete()) {
		    								// Sacamos la posición
		    								Map<Integer,Address> actualMap= nodo.getTable().getMap();
		    								List<Integer> actualIds= nodo.getTable().getArrayIds();
			    										    							
			    							int key_ = 0;
			    							for ( Integer key : actualMap.keySet() ) {			    								
			    								if(actualMap.get(key).equals(member)){
			    									key_ = key;
			    								}
			    							}

			    							//System.out.println("MAP:	"+actualMap.toString());
			    							//System.out.println("IDS:	"+actualIds.toString());
			    							//System.out.println("KEY:	"+key_);
			    							
		    								int pos_ = 0;
			    							boolean found = false;
			    							
			    							for (int i=0; i<(2*ele+1); i++){
			    								try{
				    								if (actualIds.get(i).equals(key_)){
				    									found=true;
				    								}
				    								if(found){break;}
				    								
				    							}catch(Exception e){}
			    								pos_ ++;
			    							}
			    							
			    							nodo.removeNode(member);
			    							Thread.sleep(250);
			    							
			    							if(pos_ == (2*ele+1)){}
			    							
			    							else if(pos_ < ele){
			    								//System.out.println("quiero izda " + String.valueOf(pos_));
			    								if(pos_ == 0){
			    									sendMessage(actualMap.get(actualIds.get(1)), MyHeader.SEND_LEFT_MSG, nodo.getAddress());
			    								}
			    								else{
			    									sendMessage(actualMap.get(actualIds.get(0)), MyHeader.SEND_LEFT_MSG, nodo.getAddress());
			    								}
			    							}
			    							
			    							else if(pos_ > ele){
			    								//System.out.println("quiero dcha " + String.valueOf(pos_));
			    								if(pos_== (2*ele)){
			    									sendMessage(actualMap.get(actualIds.get(2*ele-1)), MyHeader.SEND_RIGHT_MSG, nodo.getAddress());
			    								}
			    								else{
			    									sendMessage(actualMap.get(actualIds.get(2*ele)), MyHeader.SEND_RIGHT_MSG, nodo.getAddress());
			    								}
			    								
			    							}
			    							
		    							}else{
		    								nodo.removeNode(member);
		    							}
		    						}
			    				}catch(Exception e){}
		    				}
		    			}
		    		}
		    		oldView = new_view;
		    		showNodeInfo();

				} catch (Exception e) {}

		    }
		};
		
		checkClusterThread.start();
	}

	
	/**
	 * Método encargado de enviar mensajes.
	 *
	 * @param destination Vecino destinatario
	 * @param type	Tipo de paquete
	 * @param body	Contenido del paquete
	 */
	public void sendMessage(Address destination, int type, Object body){
		Thread sendMessageThread = new Thread(){
		    public void run(){
		    	try {
		    	    Message msg=new Message(destination, body);
		    	    msg.putHeader(MyHeader.magicID, new MyHeader(type));
		    		channel.send(msg);
				} catch (Exception e) {}
		    }
		};
		sendMessageThread.start();
	}

	
	/**
	 * Método que sirve para que cuando se cree un nodo, este envíe su ID a los nodos de su HT
	 * 
	 * @param msg Mensaje que avisa de que se presente
	 */
	public void introduceYourselfHandler(Message msg){
		
		try {
			Thread.sleep(800);
		} catch (InterruptedException e) {}
		
		Map<Integer, Address> actualMap = nodo.getTable().getMap();
		for ( Integer key : actualMap.keySet() ) {
			sendMessage(actualMap.get(key), MyHeader.ADD_NODE_MSG, nodo.getAddress());
		}
	}
	
	
	/**
	 * Método que sirve para enviar el ID del nodo a la izquierda del "main" a la dirección indicada en el mensaje
	 * 
	 * @param msg Mensaje que contiene la dirección a la que enviar el ID
	 */
	public void sendLeftHandler(Message msg){
		try {
			Address addr = (Address) Util.objectFromByteBuffer( msg.getBuffer() );
			Map<Integer, Address> actualMap = nodo.getTable().getMap();
			int ele = nodo.getTable().getEle();
			List<Integer> actualIds= nodo.getTable().getArrayIds();
			
			Address neededAddress = actualMap.get(actualIds.get(ele-1));
			System.out.println("Sending "+neededAddress+" to "+addr);
			sendMessage(addr, MyHeader.ADD_NODE_MSG, neededAddress);
		} catch (Exception e) {}
	}

	
	/**
	 * Método que sirve para enviar el ID del nodo a la derecha del "main" a la dirección indicada en el mensaje
	 * 
	 * @param msg Mensaje que contiene la dirección a la que enviar el ID
	 */
	public void sendRightHandler(Message msg){
		try {
			Address addr = (Address) Util.objectFromByteBuffer( msg.getBuffer() );
			Map<Integer, Address> actualMap = nodo.getTable().getMap();
			int ele = nodo.getTable().getEle();
			
			List<Integer> actualIds= nodo.getTable().getArrayIds();
			Address neededAddress = actualMap.get(actualIds.get(ele+1));
			System.out.println("Sending "+neededAddress+" to "+addr);
			sendMessage(addr, MyHeader.ADD_NODE_MSG, neededAddress);
		} catch (Exception e) {}
	}
	
	/**
	 * Método para borrar el nodo cuya ID se encuentra en el mensaje
	 * 
	 * @param msg Mensaje con el ID a borrar
	 */
	public void removeDataHandler(Message msg){
		try {
			String id = (String) Util.objectFromByteBuffer( msg.getBuffer() );
			nodo.getDataTable().removeDataFromNode(id);
		}catch (Exception e) {}
		showNodeInfo();
	}
	
	/**
	 * 
	 * @param msg
	 */
	public void rmAllHandler(Message msg){
			
		try {
			// System.out.println("rm all handler");
			String id = (String) Util.objectFromByteBuffer( msg.getBuffer() );
			nodo.getDataTable().removeDataFromNode(id);
			Map<Integer, Address> originalMap= nodo.getTable().getMap();
			List<Integer> originalIds= nodo.getTable().getArrayIds();
			for (int i = 0; i < originalIds.size(); i++){
				try{
					Address addr = originalMap.get(originalIds.get(i));
					sendMessage(addr, MyHeader.REMOVE_DATA_MSG, id);
				}catch(Exception e){}
				
			}
			showMyData();
		}catch (Exception e) {}
			
	}
	
	/**
	 * Procesamiento del paquete HELLO_MSG.
	 * Se calcula la distancia con respecto al nuevo nodo
	 * y se publica en la red dicha distancia calculada.
	 *
	 * @param msg Mensaje que contiene la el key del nodo
	 */
	private void helloHandler(Message msg){
		System.out.println(channel.getViewAsString());
		newMember = msg.getSrc();

		if(nodo.getAddress()!=newMember){ //Do not calculate distance I am the newMember
			System.out.println("HELLO MSG received from "+msg.getSrc());

			//distance = Math.abs(nodo.getKey() - (int)msg.getObject());
		    distance = Neighbours.distance(nodo.getKey(), (int)msg.getObject());

		    winner = distance; //I suppose I am winning the quorum process

		    System.out.println("Mi distancia con el nuevo es: "+distance);
		    sendMessage(null,MyHeader.DISTANCE_MSG,distance);
		}
	}

	private void newDataHandler(Message msg){
		newData = (int) msg.getObject();

		if(nodo.getKey()!=newData){ //Do not calculate distance I am the newMember
			System.out.println("NEW DATA TO BE SAVED WITH ID "+msg.getObject());
		    //distance = Math.abs(nodo.getKey() - (int)msg.getObject());
		    distance = Neighbours.distance(nodo.getKey(), (int)msg.getObject());
		    winner = distance; //I suppose I am winning the quorum process

		    System.out.println("Mi distancia con el nuevo DATO es: "+distance);
		    sendMessage(null,MyHeader.DISTANCE_MSG,distance);
		}
	}

	/**
	 * Método para enviar un dato a guardar
	 * 
	 * @param msg
	 */
	private void sendDataHandler(Message msg){
		data = msg.getObject().toString();
		if(nodo.getKey()!=newData){ //Do not calculate distance I am the newMember
			if (client==null) {
				//System.out.println("DATA TO BE SAVED IS "+msg.getObject());
			} else {
				System.out.println("REQUESTED DATA IS \""+msg.getObject()+"\" AND WAS SENT BY NODE "+msg.getSrc());
				client=null;
			}
		}
	}

	/**
	 * Método que replica los datos en los nodos de la Hashtable
	 *
	 * @param msg
	 */
	private void replicateDataHandler(Message msg){
		if (msg.getObject().toString().split("-").length==3) {
			String[] s = msg.getObject().toString().split("-");
			nodo.getDataTable().addExistingData(s[0]+"-"+s[1], s[2] );
		} else {
			nodo.getDataTable().addExistingData(msg.getObject().toString(), data);
		}

	}


	/**
	 * Recibe un mensaje con un conjunto de datos y se comprueba si estos datos se encuentran en el nodo. Si no están, se almacenan
	 *
	 * @param msg Mensaje con un conjunto de datos
	 */
	private void addCheckDataHandler(Message msg){
		try {
			Map<String, String> mapVal = (Map<String, String>) Util.objectFromByteBuffer(msg.getBuffer());
			List<Integer> checkVal = new ArrayList<Integer>();
			Set<Map.Entry<String, String>> set = mapVal.entrySet();
			for (Map.Entry<String, String> me : set) {
			  String dataKey = me.getKey().substring(0, me.getKey().indexOf('-'));
			  if (dataKey.startsWith("0")){dataKey = dataKey.substring(dataKey.indexOf("0") + 1);}
			  checkVal.add(Integer.parseInt(dataKey));
			}
			for (int val = 0; val < checkVal.size(); val++) {
				Boolean checked = nodo.checkValue(checkVal.get(val));
				if(checked == false){
					List<String> keysList = new ArrayList<String>();
					for ( String key : mapVal.keySet() ) {
						keysList.add( key );
					}
					nodo.getDataTable().addExistingData(keysList.get(val), mapVal.get(keysList.get(val)));
				}
			}
		} catch (Exception e) {}
	}


	/**
	 * Comprueba si los datos de la Datatable son correctos (viendo si tiene alguno con ID que no está en su Hashtable)
	 *
	 * @param msg Mensaje enviado a todos los nodo para que comprueben si tienen datos que deben borrar
	 */
	private void removeCheckDataHandler(Message msg){
		List<Integer> a =nodo.getTable().getArrayIds();
		nodo.getDataTable().checkRemoveData(a);
	}



	/**
	 * Obtiene los datos de su DataTable cuyo nodo más cercano es este nodo. Envía estos datos a los de su Hashtable, para que los añadan si no los tienen ya
	 *
	 * @param msg
	 */
	private void checkDataHandler(Message msg){
		Map<String, String> mapToCheck = nodo.getMyDataMap(nodo.getMain());
		Map<Integer, Address> map = nodo.getMap();
		for (int i = 0; i < nodo.getArrayIds().size(); i++) {
			if ( ( ! String.valueOf(nodo.getArrayIds().get(i)).equals( String.valueOf(nodo.getMain()) ) )  && ( nodo.getArrayIds().get(i) != null  ) ){
				if (nodo.getArrayIds().get(i) == null){
				}
				else{
					// System.out.println("HAGO CHECK, mandando a "+map.get(nodo.getArrayIds().get(i)).toString() + " el mapa "+ mapToCheck.toString());
					sendMessage(map.get(nodo.getArrayIds().get(i)), MyHeader.ADD_CHECK_DATA_MSG, mapToCheck);
				}
			}
		}

	}


	/**
	 * Envía a los nodos de su Hashtable distintos de null y del main un mensaje para que comprueben si sus datos están bien replicados
	 *
	 * También envía un mensaje a todo los nodos para que comprueben si han de borrar algún dato (útil al añadir nodos nuevos)
	 *
	 * @param msg
	 */
	private void startCheckDataHandler(Message msg){

		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {}

		Map<Integer, Address> map = nodo.getMap();

		for (int i = 0; i < nodo.getArrayIds().size(); i++) {
			if ( ( ! String.valueOf(nodo.getArrayIds().get(i)).equals( String.valueOf(nodo.getMain()) ) )  && ( nodo.getArrayIds().get(i) != null  ) ){
				if (nodo.getArrayIds().get(i) == null){}
				else{
					//System.out.println("HAZ CHECK A "+map.get(nodo.getArrayIds().get(i)).toString());
					sendMessage(map.get(nodo.getArrayIds().get(i)), MyHeader.CHECK_DATA_MSG, "");
				}
			}
		}

		try {
			Thread.sleep(200);
			sendMessage(null, MyHeader.REMOVE_CHECK_DATA_MSG, null);
		} catch (InterruptedException e) {}

	}


	/**
	 * Procesamiento del paquete DISTANCE_MSG.
	 * Al recibir la distancia calculada por un vecino entramos en estado quorum.
	 * Por cada mensaje recibido establecemos un timeout de 500ms
	 * para asegurarnos de atender a mensajes DISTANCE de nuevos vecinos.
	 * Con cada mensaje comprobamos si poseen menor distancia que la mía.
	 *
	 * @param msg Mensaje que contiene la distancia calculada por el nodo vecino
	 */
	private void distanceHandler(Message msg){
		quorum = true;
		if (newData==null && newRequest==null) counter++;
		int new_distance = (int) msg.getObject();
		if (new_distance!=0 && new_distance < distance){
			winner = new_distance;
		//This way quorum's possible draw is broken
		}else if(new_distance == distance && nodo.getTable().hashFunc(msg.getSrc()) < nodo.getKey() ){
			System.out.println("Empato pero pierdo");
			winDraw = false;
		}
	}

	/**
	 * Procesamiento del paquete ADD_NODE_MSG.
	 * Añade un nuevo nodo por la notifición de un vecino.
	 *
	 * @param msg Mensaje que contiene la dirección del nodo a añadir
	 */
	private void addNodeHandler(Message msg){
		// System.out.println("ADD_NODE MSG received from "+msg.getSrc());
		// System.out.println("Node "+msg.getObject()+" to be added");
		if (msg.getObject() == null) return;
		Address newMember = (Address) msg.getObject();
		nodo.addNode(newMember);
		Map<String, String> m = nodo.getDataTable().returnMap(nodo.getKey().toString());
		for (String i: m.keySet()) {
			sendMessage(newMember, MyHeader.REP_DATA_MSG, i+"-"+m.get(i));
		}
	}

	/**
	 * Procesamiento del paqute BYE_MSG
	 * Si el vecino que abandona se encuentra en la tabla hay que eliminarlo
	 *
	 * @param msg Mensaje que contiene el ID del nodo que abandona
	 */
	private void leaveClusterHandler(Message msg){
		if(msg.getSrc() != nodo.getAddress()){
			System.out.println("BYE MSG received from "+msg.getSrc());
			System.out.println("Node "+msg.getSrc()+" leaving cluster");
			nodo.removeNode(msg.getSrc());
		}
	}

	/**
	 * Procesamiento del paquete NEIGHBOUR_MSG
	 * Actualiza la tabla de un nuevo nodo según la información del vecino
	 *
	 * @param msg Mensaje que contiene la tabla del nodo vecino
	 */
	private void updateNeighbourHandler(Message msg){

		HashTable ht;
		try {
			ht = (HashTable) Util.objectFromByteBuffer(msg.getBuffer());
			System.out.println("NEIGHBOUR MSG received from "+msg.getSrc());
			nodo.receiveHT(ht);
		} catch (Exception e) {}

	}

	/**
	 * Maneja mensajes de tipo "SEND_TABLE_MSG". Envía su Hashtable al nodo que le indican en el cuerpo del mensaje
	 *
	 * @param msg Mensaje que contiene el destinatario al que envía la tabla
	 */
	private void sendTableHandler(Message msg){
		try {
			Address addr = (Address) Util.objectFromByteBuffer( msg.getBuffer() );
			System.out.println("'SEND YOUR TABLE' MSG received from "+msg.getSrc()+". Sending to " + addr + " ...");
			sendMessage(addr ,MyHeader.NEIGHBOUR_MSG, Util.objectToByteBuffer(nodo.getTable()));
		} catch (Exception e) {}

	}


	/**
	 * Espera con timeout que se ejecuta al entrar en estado quorum.
	 * Mediante el contador incrementamos el timeout con cada nuevo mensaje DISTANCE recibido.
	 *
	 * @throws InterruptedException
	 */
	private void setTimeOut() throws InterruptedException{
		while(counter>0){
			Thread.sleep(500);
			counter--;
		}
	}

	public static void main(String[] args) throws Exception{
		new DHT_Logic().start();

	}

}
