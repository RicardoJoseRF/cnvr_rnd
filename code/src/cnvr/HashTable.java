package cnvr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.jgroups.Address;

/**
 * 
 * @author Doris Ana Sangeap
 * @author Ignacio Domínguez Martínez-Casanueva
 * @author Ricardo José Ruiz Fernández
 *
 */

public class HashTable extends Neighbours implements java.io.Serializable {
	
	private static final long serialVersionUID = -5426085097038324946L;
	private static int ele = 2;	
	private Map<Integer, Address> map;
	private List<Integer> arrayIds;
	private Integer main = null;
	
	/**
	 * Tabla con pares clave y dirección de los nodos 
	 */
	public HashTable() {
		this.map = new TreeMap<Integer, Address>();
		this.arrayIds = new ArrayList<Integer>();
	}

	/**
	 * Sencilla función que traduce la IP a un identificador basado en el último byte
	 * 
	 * @param ipAddress Dirección IP del nodo
	 * @return La clave como un entero
	 */
	public int hashFunc(Address ipAddress){
		String[] ipAsArray = ipAddress.toString().split("-");
		int number = Integer.parseInt(ipAsArray[ipAsArray.length -1]);
		return number % 100;
	}

	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////           Métodos auxiliares          ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	/**
	 * Método auxiliar que imprime el diccionario de la tabla
	 */
	public void printDict(){
		System.out.println("HASHTABLE:   " + (map.toString()) + "  (Tamaño:"+String.valueOf(map.size())+")");	
	}
	
	
	/**
	 * Método auxiliar que imprime el listado de claves 
	 */
	public void printArrayIds() {
		for (int i=0; i<arrayIds.size(); i++) {
			if (i==(arrayIds.size()-1)){
				System.out.print(" " + arrayIds.get(i) + " ");
			}
			else{
				System.out.print(" " + arrayIds.get(i) + " | ");
			}		
		}
		System.out.println("");
	}
	
	
	/**
	 * 
	 * Función que comprueba si la tabla contiene valores nulos
	 * 
	 * @return Boolean: true si contiene un elemento nulo, false en caso contrario
	 */
	public boolean tableIsComplete(){
		return !arrayIds.contains(null);
	}

	/**
	 * Dado un mapa y una lista, pone los elementos de la tabla en la lista
	 * 
	 * @param m
	 * @param l
	 */
	public static void getArrayIdsFromMap(Map<Integer, Address> m, List<Integer> l) {	
		for ( Integer key : m.keySet() ) {
			l.add( key );
		}		
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////              Añadir nodo              ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	/**
	 * Da el número de elementos de la tabla que son mayores que main
	 * 
	 * @param l Listado de claves
	 * @return
	 */
	public int greaterMain(List<Integer> l) {
		int n = 0;
		for (int i=0; i<l.size(); i++) {
			if ((l.get(i)!=null)&&(l.get(i) > main)) n++;
		}
		return n;
	}
	
	/**
	 * Da el número de elementos de la tabla que son menores que main
	 * 
	 * @param l Listado de claves
	 * @return
	 */
	public int lesserMain(List<Integer> l) {
		int n = 0;
		for (int i=0; i<l.size(); i++) {
			if ((l.get(i)!=null)&&(l.get(i) < main)) n++;
		}
		return n;
	}
	
	
	/**
	 * Rota los elementos de una tabla añadiendo su primer elemento al final
	 * y quitándoselo del principio hasta que el main de la tabla queda en la
	 * posición "ele"
	 * 
	 * @param l
	 * @param greater
	 */
	public void rotateAux(List<Integer> l, boolean greater) {
		if (greater == true) {
			while (l.get(ele) != main) {
				l.add(l.get(0));
				l.remove(l.get(0));
			}
		} else {
			while (l.get(l.size()-ele-1) != main) {
				l.add(l.get(0));
				l.remove(l.get(0));
			}
		}
	}
	
	/**
	 * Utiliza el método rotateAux para rotar los elementos de la tabla
	 * considerando los distintos casos de llenado de la tabla
	 * 
	 * @param l Listado de claves
	 * @return Clave expulsada de la lista
	 */
	public Integer rotate(List<Integer> l) {
		int nNulls = (2*ele+1) - l.size();
		Integer excluded = null;
		if (l.size() < 2 * (ele + 1)) {
			if ((greaterMain(l) < ele + 1) && (lesserMain(l) < ele + 1)) {
				for (int i = 0; i < nNulls; i++) {
					l.add(null);
				}
				rotateAux(l, true);
			} else if (lesserMain(l) > ele) {
				rotateAux(l, true);
				for (int i = 0; i < nNulls; i++) {
					l.add(null);
				}
			} else if (greaterMain(l) > ele) {
				rotateAux(l, false);
				List<Integer> nulls = new ArrayList<Integer>();
				nulls.add(null);
				for (int i = 0; i < nNulls; i++) {
					l.addAll(0, nulls);
				}
			}
		} else {
			rotateAux(l, true);
			excluded = l.get(l.size()-1);
			l.remove(excluded);
		}
		return excluded;
	}
	

	
	/**
	 * Coge el mapa, le añade el nodo con Address(String) a, ordena la nueva lista
	 * y devuelve un array con los elementos de la lista ordenados en orden creciente
	 * 
	 * @param a Dirección
	 * @param key Clave
	 * @param m Mapa
	 * @return Array con los elementos de la lista ordenados en orden creciente
	 */
	public List<Integer> order(Address a, Integer key, Map<Integer, Address> m) {
		m.put(key, a);
		List <Integer> nueva = new ArrayList<Integer>();
		getArrayIdsFromMap(m, nueva);
		return nueva;
	}
	
	
	/**
	 * Coge el mapa, y si existe el nodo con clave Integer key se lo quita, ordena la 
	 * nueva lista y devuelve un array con los elementos de la lista ordenados en orden 
	 * creciente
	 *   
	 * @param key Clave del nodo a eliminar
	 * @param m Mapa
	 * @return Array con los elementos de la lista ordenados en orden creciente
	 */
	public List<Integer> order1(Integer key, Map<Integer, Address> m) {
		m.remove(key);
		List <Integer> nueva = new ArrayList<Integer>();
		getArrayIdsFromMap(m, nueva);
		return nueva;
	}
	
	
	/**
	 * Añade un nodo a la tabla y devuelve una lista en la que:
	 * 1er elemento: Address(String) del nodo excluido de la tabla
	 * (null mientras haya menos de (2*ele+1) nodos en la tabla
	 * 
	 * 2o elemento: Address(String) del nodo extremo de la tabla
	 * más cercano al excluido
	 * 
	 * Si la tabla está vacía añade el primer elemento en posición "ele"
	 * y lo nombra main de la tabla
	 * 
	 * Si en la tabla ya hay al menos un nodo, lo añade y situa en la
	 * posición adecuada haciendo uso del método rotate
	 * 
	 * @param ipAddress Dirección del nodo a añadir
	 * @return Devuelve una lista donde: 1º Dirección nodo excluido y 2º Vecino destinatario
	 */
	public List<Address> addNode(Address ipAddress) {
		
		Integer nodeId = hashFunc(ipAddress);
		Integer excluded = null;
		List<Integer> lNodes = order(ipAddress, nodeId, map);

		if (lNodes.size() == 1) {
			main = nodeId;
			System.out.println("Creo el nodo con main: "+main);
			lNodes.clear();
			for (int i=0; i<2*ele+1; i++) lNodes.add(null);
			lNodes.set(ele, nodeId);		
		} else {
			excluded = rotate(lNodes);
		}
		arrayIds = lNodes;
		///System.out.println("El excluido es: "+excluded);
		
		Address nearest = null;
		Address excIpAdd = null;
		if (excluded != null) {
			nearest = map.get(nearestNode(excluded, arrayIds));
			excIpAdd = map.get(excluded);
			map.remove(excluded);
		}
		List<Address> l = new ArrayList<Address>();
		l.add(excIpAdd);
		l.add(nearest);
		return l;
	}

	
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////       Borrar nodo y reajustar tabla de los vecinos      ////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Elimina un nodo de la tabla en caso de que este exista rellenando la posicion
	 * en la que se encontraba dentro de la tabla con "null". Devuelve TRUE si el nodo
	 * exist�a y se ha eliminado y FALSE en varios casos:
	 * - el nodo a eliminar no existia en la tabla
	 * - el nodo a eliminar es el main de la tabla
	 * 
	 * @param ipAddress Direcci�n del nodo a eliminar
	 * @return Boolean que nos indica si se ha eliminado o no el nodo propuesto
	 */
	public Boolean removeNode(Address ipAddress) {
		Integer nodeId = hashFunc(ipAddress);
		Boolean removed = false;
		if (map.containsKey(nodeId) && (nodeId != main)) {
			removed = true;
			List<Integer> lNodes = order1(nodeId, map);
			if (lNodes.size() == 1) {
				System.out.println("La tabla del nodo con main: "+main+" se ha quedado vac�a");
				lNodes.clear();
				for (int i=0; i<2*ele+1; i++) lNodes.add(null);
				lNodes.set(ele, main);			
			} else {
				System.out.println("Se ha eliminado el nodo: "+nodeId);
				rotate(lNodes);	
			}
			arrayIds = lNodes;
		} else {
			if (nodeId == main) System.out.println("No se puede eliminar el main de la tabla");
			else System.out.println("El nodo: "+nodeId+" no existe en la tabla del nodo: "+main);
		}	
		
		return removed;
		
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////       Crear tablas en nodo nuevo      ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 
	 * Método para decidir si te llega la Ht por la derecha o por la izquierda. Recibo una HT ya actualizada y  calculo si 
	 * es el de la derecha si tiene el main en el puesto 0. Si no, es el de la izda.
	 * 
	 * @param ht
	 */
	public void receiveHT(HashTable ht){
		
		Map<Integer, Address> receivedMap 	= ht.getMap();
		
		for ( Integer key : receivedMap.keySet() ) {
			boolean found = false; 
			for (Address value: map.values()) {
				if(receivedMap.get(key).equals(value)){
					found = true;
				}
			}
			if(!found){
				addNode(receivedMap.get(key));
			}			
		}	

	} 
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////            Setters y getters          ///////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	public Map<Integer, Address> getMap() {
		return map;
	}

	public void setMap(Map<Integer, Address> map) {
		this.map = map;
	}

	public List<Integer> getArrayIds() {
		return arrayIds;
	}

	public void setArrayIds(List<Integer> arrayIds) {
		this.arrayIds = arrayIds;
	}

	public Integer getMain() {
		return main;
	}

	public void setMain(Integer main) {
		this.main = main;
	}
	
	public int getEle(){
		return ele;
	}
	

}
