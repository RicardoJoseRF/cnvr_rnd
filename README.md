# Manual de instalación #

##
##

##
##

## 1 - SSH con Bitbucket
##

*1.1 Comprobar si ya hemos creado una clave pública (Fuente: http://www.solocodigoweb.com/blog/2014/04/03/configurar-ssh-para-bitbucket/):
*

cat ~/.ssh/id_rsa.pub 

*1.2 Si no tenemos aún, ejecutamos:
*

ssh-keygen

*1.3 E introducimos la clave en nuestra cuenta de Bitbucket
*

##2 - Descarga de repositorio
##

*A continuación creamos un directorio en $HOME y clonamos el repositorio:
*

cd $HOME

mkdir cnvr

cd cnvr

git clone git@bitbucket.org:RicardoJoseRF/cnvr_rnd.git



##3 - Instalación de Eclipse
##

*3.1 Instalar Java 8 (Fuente: http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html)
*

sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer

*3.2 Descargar Eclipse Installer desde el navegador por ejemplo:
*

https://www.eclipse.org/downloads/download.php?file=/oomph/epp/neon/R1/eclipse-inst-linux64.tar.gz&mirror_id=63

*3.3 Ir a la carpeta de Descargas o Downloads, descomprimir e instalar:
*

cd Descargas 

sudo tar -zxvf eclipse*

cd eclipse-installer/

./eclipse-inst

*3.4 Instalar el de Java EE. Aceptar a todo
*

*3.5. Crear o cambiar este fichero:
*

sudo nano /usr/share/applications/eclipse.desktop

*Y poner (ojo a la ruta $HOME, no es válida. Hay que poner la ruta al directorio donde se ha descargado eclipse):
*

[Desktop Entry]

Name=Eclipse

Type=Application

Exec=env UBUNTU_MENUPROXY= $HOME/eclipse/jee-neon/eclipse/eclipse

Terminal=false

Icon=$HOME/eclipse/jee-neon/eclipse/icon.xpm

Comment=Integrated Development Environment

NoDisplay=false

Categories=Development;IDE;

Name[en]=eclipse.desktop

X-Desktop-File-Install-Version=0.22

##4 - Importar proyecto y librería jGroups
##

*4.1. Abrimos Eclipse > File > New > Java Project. Desmarcamos "Use default location"y navegamos hasta el directorio $HOME/cnvr/cnvr_rnd/code. Pinchamos en Finish
*

*4.2. Hacemos click derecho sobre el proyecto. Vamos a Propiedades > Java Build Path > Pestaña "Libraries" > Add external JARs. Añadimos la versión 3.6.11 que está en /util/JGroups del proyecto.
*

##5 - Generar el fichero .jar
##

*Generar el .jar con el código propio desde Eclipse:

File - Export - Runnable JAR - Seleccionar DHT_Logic y guardar donde queramos con nombre experimento.jar por ejemplo
*

##6 - Comandos en cada terminal empleado
##

*6.1. A mano*

export CLASSPATH=$CLASSPATH:<path_al_fichero>/experimento.jar

java -Djava.net.preferIPv4Stack=true cnvr.DHT_Logic

*Al iniciarse el programa introducir un identificador con el siguiente formato:
*

nombre-decimalEntre0y99

*Al iniciarse el nodo se le puede añadir un dato escribiendo cualquier cadena que no sea "", empiece por "data"/"exit"/"quit"
*

*Desde cualquier otro nodo, la información almacenada en el nodo anterior puede recuperarse usando el siguiente formato:
*

data-decimalEntre0y99 (tiene que ser la primera parte del id del dato de antes)

*6.2.  Usando el script
*

*Ir hasta el directorio en el que tengamos el script "script.sh" (se encuentra en la carpeta /util del respositorio) y ejecutar:*

chmod 777 script.sh


*Por último, introducir la ruta del jar generado en el punto 5 y el número de nodos a crear*


Uso:	./script.sh {ruta jar} {nº máquinas}

Ej:	./script.sh /home/ricardo/aaa/util/cnvr.jar 1

