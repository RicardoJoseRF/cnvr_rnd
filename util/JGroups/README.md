# Instalación JGroups:

 1. Descargamos el .jar de JGroups
 2. Nos asegurarnos de tener una versión actualizada del JDK de Java (actualmente usando Java1.8)
 3. Configurar Eclipse para emplear la librería JAR externa _jgroups-3.6.11.Final.jar_ (modificando Java BuildPath) del proyecto
 4. Generar el .jar con el código propio desde Eclipse
 > File - Export - Runnable JAR - Seleccionar DHT_Logic y guardar donde queramos con nombre experimento.jar por ejemplo
 > _IMPORTANTE: Ejecutar los siguientes pasos en cada terminal empleado_ 
 5. En sistemas Unix (Linux/Mac OS) configuramos el classpath de Java para que apunte al jar de JGroups. Para ello ejecutamos el siguiente comando:
    ```
    export CLASSPATH=$CLASSPATH:<path_al_fichero>/experimento.jar
    ```     
 6. Ahora podemos ejecutar la aplicación java con el siguiente comando (Se ejecuta la demo Draw dentro del host):
    ```
    java -Djava.net.preferIPv4Stack=true cnvr.DHT_Logic
    ```
 7. Al iniciarse el programa introducir un identificador con el siguiente formato:
 > nombre-decimalEntre0y99
